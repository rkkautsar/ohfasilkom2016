document.addEventListener('DOMContentLoaded', function() {
  window.jQuery = window.$ = require('jquery');

  window.ScrollMagic = require('scrollmagic');
  require('scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap');
  require('scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators');
  window.TweenMax = require('gsap');
  window.TimelineMax = TweenMax.TimelineMax;

  require('fullpage.js');
  require('jquery-unslider');
  require('js/rotator.js');
  require('js/scroll.js');
  require('js/navbar.js');
  require('js/quotes.js');
  require('js/countdown.js');
  require('js/maps.js');
  require('js/slider.js');
});
