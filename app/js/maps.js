var bittersMap = (function () {
  var myLatlng = new google.maps.LatLng(-6.3646009, 106.8286886),
      mapCenter = new google.maps.LatLng(-6.3646009, 106.8286886),
      mapCanvas = document.getElementById('map_canvas'),
      mapOptions = {
        center: mapCenter,
        zoom: 16,
        scrollwheel: false,
        draggable: true,
        disableDefaultUI: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      },
      map = new google.maps.Map(mapCanvas, mapOptions),
      marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: 'OH Fasilkom 2016'
      });

  return {
    init: function () {
      map.set('styles', [{
        featureType: 'landscape',
        elementType: 'geometry',
        stylers: [
          { hue: '#ffff00' },
          { saturation: 30 },
          { lightness: 10}
        ]}
      ]);

      google.maps.event.addListener(marker, 'click', function () {
        window.location = "https://goo.gl/maps/dcafaBAbsmn";
      });
    }
  };
}());

bittersMap.init();
