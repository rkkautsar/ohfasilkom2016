var Tminus = require('lib-tminus');

$(function() {
  var control = Tminus.countdown({
    endTime: new Date('Sat, Nov 12, 2016 08:30:00 GMT+0700'),
    target: $('#countdown'),
  });
  control.start();
});