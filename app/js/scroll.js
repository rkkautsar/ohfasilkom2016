$(function() {

  // handle links with @href started with '#' only
  $(document).on('click', 'a[href^="#"]', function(e) {
      // target element id
      var id = $(this).attr('href');

      // target element
      var $id = $(id);
      if ($id.length === 0) {
          return;
      }

      // prevent standard hash navigation (avoid blinking in IE)
      e.preventDefault();

      // top position relative to the document
      var pos = $(id).offset().top;

      // animated top scrolling
      $('body, html').animate({scrollTop: pos});
  });


  var controller = new ScrollMagic.Controller({
      globalSceneOptions: {
        triggerHook: 'onLeave'
      }
    });

  var heroTween = new TimelineMax()
    .add([
      TweenMax.to('#balloon-1', 8, { bottom: -250, opacity: 0.2 }),
      TweenMax.to('#balloon-2', 10, { bottom: -900, opacity: 0.2 }),
      TweenMax.to('#satellite-1', 16, { bottom: -660, opacity: 0.2 }),
    ]);

  new ScrollMagic.Scene({
      triggerElement: '.hero',
      duration: '200%',
      reverse: true,
    })
    .setTween(heroTween)
    //.addIndicators()
    .addTo(controller);

  var navigationTween = new TimelineMax()
    .add([
      TweenMax.fromTo('.navigation', 0.125, { display: 'none', opacity: 0 }, { display: 'block', opacity: 1 }),
    ]);

  new ScrollMagic.Scene({
      triggerElement: '#welcome',
      offset: 0,
      duration: 0,
      reverse: true,
    })
    .setTween(navigationTween)
    //.addIndicators()
    .addTo(controller);

  var welcomeTween = new TimelineMax()
    .add([
      TweenMax.staggerFromTo('#welcome .bullet-content', 0.5, { opacity: 0, y: 20 }, { opacity: 1, y: 0 }, 0.15),
    ]);

  new ScrollMagic.Scene({
      triggerElement: '#welcome',
      offset: -300,
      duration: 0,
      reverse: false,
    })
    .setTween(welcomeTween)
    //.addIndicators()
    .addTo(controller);


  var aboutTween = new TimelineMax()
    .add([
      TweenMax.staggerFromTo('#about .bullet-content', 0.5, { opacity: 0, y: 20 }, { opacity: 1, y: 0 }, 0.15),
    ]);

  new ScrollMagic.Scene({
      triggerElement: '#about',
      offset: -300,
      duration: 0,
      reverse: false,
    })
    .setTween(aboutTween)
    //.addIndicators()
    .addTo(controller);

  var timelineTween = new TimelineMax()
    .add([
      TweenMax.staggerFromTo('#timeline .item', 0.5, { opacity: 0, scale: 0.8 }, { opacity: 1, scale: 1 }, 0.15),
    ]);

  new ScrollMagic.Scene({
      triggerElement: '#timeline',
      offset: -300,
      duration: 0,
      reverse: false,
    })
    .setTween(timelineTween)
    //.addIndicators()
    .addTo(controller);

  var logosTween = new TimelineMax()
    .add([
      TweenMax.staggerFromTo('#logos img', 0.5, { opacity: 0, scale: 0.8 }, { opacity: 1, scale: 1 }, 0.15),
    ]);

  // new ScrollMagic.Scene({
  //     triggerElement: '#logos',
  //     offset: -300,
  //     duration: 0,
  //     reverse: false,
  //   })
  //   .setTween(logosTween)
  //   //.addIndicators()
  //   .addTo(controller);
});