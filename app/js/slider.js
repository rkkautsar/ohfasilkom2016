$(function() {
  $('.slider').unslider({
    arrows: {
      prev: '<a class="unslider-arrow prev">&lt;</a>',
      next: '<a class="unslider-arrow next">&gt;</a>',
    },
    autoplay: false,
    nav: false,
  });
});